using System;
using System.Threading.Tasks;
using System.Diagnostics;

using ToDoItemsApp.Helpers;
using ToDoItemsApp.Interfaces;
using ToDoItemsApp.Droid.Helpers;

using Microsoft.WindowsAzure.MobileServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(Authentication))]
namespace ToDoItemsApp.Droid.Helpers
{
    public class Authentication : IAuthentication
    {
        public async Task<MobileServiceUser> LoginAsync(MobileServiceClient client, MobileServiceAuthenticationProvider provider)
        {
            try
            {
                Debug.WriteLine("LoginAsync (1)");

                Settings.LoginAttempts++;
                MobileServiceUser user = await client.LoginAsync(Forms.Context, provider);
                Debug.WriteLine("LoginAsync (2)");

                Settings.AuthToken = (! string.IsNullOrEmpty (user.MobileServiceAuthenticationToken)) ? user.MobileServiceAuthenticationToken : string.Empty;
                Settings.UserId = (!string.IsNullOrEmpty(user.UserId)) ? user.UserId : string.Empty;
                Debug.WriteLine("LoginAsync (Done)");

                return user;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("LoginAsync FAILED ?!?! -- {0}", ex.Message);
            }

            return null;
        }

        public async Task LogoutAsync(MobileServiceClient client)
        {
            try
            {
                Debug.WriteLine("LogoutAsync (1)");
                await client.LogoutAsync();
                Debug.WriteLine("LogoutAsync (2)");

                // clear settings
                Settings.AuthToken = String.Empty;
                Settings.UserId = String.Empty;
                Settings.LoginAttempts = 0;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("LogoutAsync failed ?!?! -- {0}", ex.Message);
            }
            finally
            {
                Debug.WriteLine("LogoutAsync (Done)");
            }
        }
        
        public void ClearCookies()
        {
            try
            {
                if ((int)global::Android.OS.Build.VERSION.SdkInt >= 21)
                    global::Android.Webkit.CookieManager.Instance.RemoveAllCookies(null);
            }
            catch (Exception)
            {
                Debug.WriteLine("ClearCookies failed ?!?!");
            }
        }
    }
}