﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoItemsApp.Services
{

    [DataTable("ToDoItem")]
    public class ToDoItem
    {
        // system properties
        [JsonProperty("userId")]
         public String UserId { get; set; }

        [JsonProperty("id")]
        public String Id { get; set; }

        [Version]
        public String AzureVersion { get; set; }

        // user defined properties
        [JsonProperty(PropertyName = "Description")]
        public String Description { get; set; }

        [JsonProperty(PropertyName = "Priority")]
        public int Priority { get; set; }

        [JsonProperty(PropertyName = "Expiration")]
        public DateTime Expiration { get; set; }

        [JsonProperty(PropertyName = "OS")]
        public String OS { get; set; }

        [JsonIgnore]
        public String DateDisplay { get { return this.Expiration.ToLocalTime().ToString("d"); } }

        [JsonIgnore]
        public String TimeDisplay { get { return this.Expiration.ToLocalTime().ToString("t"); } }

        public override String ToString()
        {
            return String.Format(
                "ToDoItem: Description={0}, Priority={1}, Expiration={2}",
                this.Description, this.Priority.ToString(), this.Expiration);
        }
    }
}
