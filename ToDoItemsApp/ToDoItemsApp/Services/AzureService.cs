using System;
using System.Diagnostics;
using System.Threading.Tasks;

using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using ToDoItemsApp.Helpers;
using Xamarin.Forms;

namespace ToDoItemsApp.Services
{
    public class AzureService
    {
        private const String path = "itemssyncstore.db";

        private IMobileServiceSyncTable<ToDoItem> localTable;

        public MobileServiceClient MobileService { get; set; }

        private bool isInitialized;

        public AzureService()
        {
            Debug.WriteLine("AzureService c'tor");

            this.isInitialized = false;
        }

        public async Task Initialize()
        {
            if (this.isInitialized)
                return;

            Debug.WriteLine("AzureService Initialize ...");

            // await Task.Delay(0);

            AuthHandler handler = new AuthHandler();

            this.MobileService = new MobileServiceClient("http://todoitemsapp.azurewebsites.net", handler);
            // this.MobileService = new MobileServiceClient("http://todoitemsapp.azurewebsites.net");



            handler.Client = MobileService;

            if (!String.IsNullOrWhiteSpace(Settings.AuthToken) && !String.IsNullOrWhiteSpace(Settings.UserId))
            {
                this.MobileService.CurrentUser = new MobileServiceUser(Settings.UserId);
                this.MobileService.CurrentUser.MobileServiceAuthenticationToken = Settings.AuthToken;
            }

            // setup local sqlite store and initialize local table
            MobileServiceSQLiteStore store = new MobileServiceSQLiteStore(path);
            Debug.WriteLine("Created SQLite database ...");

            store.DefineTable<ToDoItem>();
            Debug.WriteLine("Defined table in the SQLite database ...");

            // need synchronization context
            // await this.MobileService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
            await this.MobileService.SyncContext.InitializeAsync(store);
            Debug.WriteLine("Initialized synchronization context ...");

            this.localTable = this.MobileService.GetSyncTable<ToDoItem>();

            this.isInitialized = true;

            Debug.WriteLine("AzureService Initialize: Done");
        }

        public async Task AddToDoItem(String description, int prio)
        {
            await this.Initialize();

            // create and insert todo item
            var item = new ToDoItem
            {
                Expiration = DateTime.UtcNow,
                Description = description,
                Priority = prio,
                OS = Device.OS.ToString()
            };

            Debug.WriteLine ("Added item {0}", item);

            await this.localTable.InsertAsync(item);

            await SyncToDoItems();
        }

        public async Task SyncToDoItems()
        {
            Debug.WriteLine("Synchronize with backend ...");

            try
            {
                // pull down all latest changes and then push current coffees up
                await this.localTable.PullAsync("allToDoItems", this.localTable.CreateQuery());

                // TODO: Hmmm ?!?!
                // await MobileService.SyncContext.PushAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to sync items - maybe we're Offline [{0}]", ex.Message);
            }

            Debug.WriteLine("Synchronize with backend: Done!");
        }
    }
}
