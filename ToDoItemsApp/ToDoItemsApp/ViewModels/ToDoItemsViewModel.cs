﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToDoItemsApp.Helpers;
using ToDoItemsApp.Interfaces;
using ToDoItemsApp.Services;
using Xamarin.Forms;

namespace ToDoItemsApp.ViewModels
{
    public class ToDoItemsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isBusy;
        private String loadingMessage;
        private String todoItemText;
        private String todoItemPrio;

        private AzureService azureService;

        public ToDoItemsViewModel()
        {
            Debug.WriteLine("ToDoItemsViewModel c'tor");

            this.azureService = new AzureService();

            this.loadingMessage = "";
            this.todoItemText = "";
            this.todoItemPrio = "";
        }

        // properties
        public String LoadingMessage
        {
            get
            {
                return this.loadingMessage;
            }

            set
            {
                if (this.loadingMessage != value)
                {
                    this.loadingMessage = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(
                            this, new PropertyChangedEventArgs("LoadingMessage"));
                    }
                }
            }
        }

        public String TodoItemText
        {
            get
            {
                return this.todoItemText;
            }

            set
            {
                if (this.todoItemText != value)
                {
                    this.todoItemText = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(
                            this, new PropertyChangedEventArgs("TodoItemText"));
                    }
                }
            }
        }

        public String TodoItemPrio
        {
            get
            {
                return this.todoItemPrio;
            }

            set
            {
                if (this.todoItemPrio != value)
                {
                    this.todoItemPrio = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(
                            this, new PropertyChangedEventArgs("TodoItemPrio"));
                    }
                }
            }
        }

        // commands
        public ICommand AddItemCommand
        {
            get
            {
                return new Command(async () => await ExecuteAddItemCommandAsync()); 
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                return new Command(async () => await ExecuteLogoutCommandAsync()); 
            }
        }


        private async Task ExecuteAddItemCommandAsync()
        {
            String userId = String.Empty;

            if (this.isBusy)
            {
                Debug.WriteLine("Service is BUSY !");
                return;
            }

            this.isBusy = true;

            try
            {
                if (!Settings.IsLoggedIn())
                {
                    await this.azureService.Initialize();

                    IAuthentication authenticator = DependencyService.Get<IAuthentication>();

                    MobileServiceUser user = await authenticator.LoginAsync(this.azureService.MobileService, MobileServiceAuthenticationProvider.MicrosoftAccount);
                    if (user == null)
                    {
                        Debug.WriteLine("LoginAsync returned with ERROR");
                        return;
                    }
                    else
                    {
                        userId = user.UserId;
                        Debug.WriteLine("    LoginAsync returned with SUCCESS: UserId = >>>{0}<<<", userId);
                        Debug.WriteLine("    MobileServiceAuthenticationToken:  {0}", user.MobileServiceAuthenticationToken);
                    }
                }

                // this.LoadingMessage = "Loading Todo Items ...";

                //String description = this.TodoItemText;
                //String priority = this.TodoItemPrio;
                //int result;
                //Int32.TryParse(priority, out result);
                //if (result == 0)
                //    result = 9;   // lowest prio


                // MÖCHTE ERST MAL NICH AUF ZURE ZUGREIFEN !!!

                //String description = "Hans ist jetzt dran";
                //int result = 99;

                //await this.azureService.AddToDoItem(description, result);

                //Debug.WriteLine("Yeah !!!! ===> >>> {0} <<<", userId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("OH NO!" + ex.Message);
            }
            finally
            {
                this.isBusy = false;
            }
        }

        private async Task ExecuteLogoutCommandAsync()
        {
            Debug.WriteLine("Logout Command Async");

            if (! Settings.IsLoggedIn())
            {
                Debug.WriteLine("User is NOT logged in !");
                return;
            }

            if (this.isBusy)
            {
                Debug.WriteLine("Service is BUSY !");
                return;
            }

            this.isBusy = true;

            try
            {
                //What's wrong with that??
                await this.azureService.Initialize();

                IAuthentication authenticator = DependencyService.Get<IAuthentication>();

                await authenticator.LogoutAsync(this.azureService.MobileService);

                Debug.WriteLine("LogoutAsync Done");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("OH NO!" + ex);
            }
            finally
            {
                this.isBusy = false;
            }
        }
    }
}
