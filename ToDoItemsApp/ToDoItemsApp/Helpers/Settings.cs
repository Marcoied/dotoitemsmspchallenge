using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;

namespace ToDoItemsApp.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings;

        static Settings()
        {
            AppSettings = CrossSettings.Current;
        }

        const String UserIdKey = "userid";
        static readonly String UserIdDefault = String.Empty;

        const String AuthTokenKey = "authtoken";
        static readonly String AuthTokenDefault = String.Empty;

        const string LoginAttemptsKey = "login_attempts";
        const int LoginAttemptsDefault = 0;

        public static String AuthToken
        {
            get { return AppSettings.GetValueOrDefault<String>(AuthTokenKey, AuthTokenDefault); }
            set { AppSettings.AddOrUpdateValue<String>(AuthTokenKey, value); }
        }

        public static String UserId
        {
            get { return AppSettings.GetValueOrDefault<String>(UserIdKey, UserIdDefault); }
            set { AppSettings.AddOrUpdateValue<String>(UserIdKey, value); }
        }

        public static int LoginAttempts
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(LoginAttemptsKey, LoginAttemptsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(LoginAttemptsKey, value);
            }
        }

        public static bool IsLoggedIn()
        {
            return !String.IsNullOrWhiteSpace(UserId);
        } 
    }
}